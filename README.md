
👽 Micronaut Spock(Groovy) GraalVM Hexagonal Architecture (SOLID TDD  BDD)
===============


[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://github.com/spockframework/spock/blob/master/LICENSE)
[![Maven Central](https://img.shields.io/maven-central/v/org.spockframework/spock-core.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:org.spockframework)
[![GitHub Workflow Status (branch)](https://img.shields.io/github/workflow/status/spockframework/spock/Build%20and%20Release%20Spock/master)](https://github.com/spockframework/spock/actions?query=workflow%3A%22Build+and+Release+Spock%22+branch%3Amaster)
[![Jitpack](https://jitpack.io/v/org.spockframework/spock.svg)](https://jitpack.io/#org.spockframework/spock)
[![Codecov](https://codecov.io/gh/spockframework/spock/branch/master/graph/badge.svg)](https://codecov.io/gh/spockframework/spock)
[![Gitter](https://badges.gitter.im/spockframework/spock.svg)](https://gitter.im/spockframework/spock?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![Revved up by Gradle Enterprise](https://img.shields.io/badge/Revved%20up%20by-Gradle%20Enterprise-06A0CE?logo=Gradle&labelColor=02303A)](https://ge.spockframework.org/scans)


🎳 Micronaut 3.3.4 Documentation
---
- [Micronaut in Action](https://lufgarciaqu.medium.com/micronaut-en-acci%C3%B3n-4781d80890c0)
- [User Guide](https://docs.micronaut.io/3.3.1/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.3.1/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.3.1/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)

👌 Hexagonal Architecture
---
The Hexagonal Architecture is a software architecture that is based on the [Clean Architecture](https://clean-architecture.org/) pattern.
- [Intro Hexagonal Architecture in Spanish ](https://lufgarciaqu.medium.com/arquitectura-hexagonal-b40d3f759ac9)
  ![img_1.png](https://miro.medium.com/max/1400/1*vokISpyLCTr4BV9DgkDNNA.png)


💊 Spock Framework
---
Spock is a BDD-style developer testing and specification framework for Java and [Groovy](https://groovy-lang.org/) applications.
To learn more about Spock, visit https://spockframework.org. To run a sample spec in your browser, go to
https://meetspock.appspot.com/.
[![Types Test](https://miro.medium.com/max/1400/1*yvkcmS-vZdx2MBifjNd5zA.png)

- [Micronaut+Spock other level](https://lufgarciaqu.medium.com/spock-testing-a-otro-nivel-af05ad2516ea)
- [User Guide](https://github.com/spockframework/spock)

🤯 Remind to Tests
---
This project apply TDD, BDD and too:
- [X] Acceptance Testing
- [X] Integration Testing
- [X] Unit Testing




🚀 Micronaut + Graal [![Latest release](https://img.shields.io/github/v/release/graalvm/graalvm-ce-builds?color=brightgreen&label=latest%20release)](https://github.com/graalvm/graalvm-ce-builds/releases/latest)
---
GraalVM is a high performance runtime for Java, JavaScript, LLVM-based languages such as C and C++, and other dynamic languages. Additionally, GraalVM allows efficient interoperability between programming languages and compiling Java applications ahead-of-time into native executables for faster startup time and lower memory overhead.

### ☁️ Why GraalVM?

[![Types Test](https://miro.medium.com/max/1400/1*HHZvXgn3RTZMK8IO9KBgLw.png)](https://lufgarciaqu.medium.com/micronaut-en-acci%C3%B3n-4781d80890c0)

- This repository hosts [GraalVM CE](https://github.com/oracle/graal/) [builds](https://github.com/graalvm/graalvm-ce-builds/releases/) based on OpenJDK.



🔌 Feature http-client documentation
---

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

📟 Execute any commands
---

### Running tests
```shell
./gradlew test 
```
For more details, aggregate `--stacktrace`
### Building with GraalVM

```shell
 ./gradlew nativeCompile --scan
```
For more details, aggregate `--scan`
### Building without GraalVM
```shell
./gradlew build 
``` 
For more details, aggregate `--debug`

## 🚧 UML

### Package Diagram
![structure](./src/main/resources/static/img/structure.png)

### Class Diagram
![main](./src/main/resources/static/img/main.png)

😎 By:
---
* [Luis Fernando Garcia `👽`](https://lufgarciaqu.medium.com/)

