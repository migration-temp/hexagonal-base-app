#!/usr/bin/env bash

echo "Downloading GraalVM"

wget -q https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.0.0.2/graalvm-ce-java17-linux-amd64-22.0.0.2.tar.gz

tar zxf graalvm-ce-java17-linux-amd64-22.0.0.2.tar.gz

echo "Installing GraalVM via gu"

${CI_PROJECT_DIR}/graalvm-ce-java17-22.0.0.2/bin/gu install native-image
