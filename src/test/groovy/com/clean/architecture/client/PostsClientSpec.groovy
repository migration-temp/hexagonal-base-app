package com.clean.architecture.client

import com.clean.architecture.contexts.posts.application.find.PostsFinder
import com.clean.architecture.contexts.posts.domain.PostsQuery
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MockBean
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Specification

@MicronautTest(environments = ["test"])
class PostsClientSpec  extends Specification {

    @Inject
    PostsFinder postsFinder

    @Inject
    @Client('/')
    HttpClient client

    static final responseMock =[{
        1
        1
        "title"
        "body"
    }]


    def 'should return information posts'(){
        when:
        def result = client.toBlocking().retrieve(HttpRequest.GET('posts'), List<PostsQuery>.class)
        then:
        1* postsFinder.run() >> responseMock
        result.size()>0
    }

    @MockBean(PostsFinder)
    PostsFinder postsFinder() {
        Mock(PostsFinder)
    }
}
