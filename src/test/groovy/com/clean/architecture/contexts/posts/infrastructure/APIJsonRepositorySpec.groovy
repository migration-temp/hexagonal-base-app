package com.clean.architecture.contexts.posts.infrastructure

import com.clean.architecture.contexts.posts.domain.PostsQuery
import com.clean.architecture.contexts.posts.domain.PostsRepository
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.http.simple.SimpleHttpResponse
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Specification
import spock.lang.Subject

@MicronautTest(environments = ["test"])
class APIJsonRepositorySpec extends Specification{

  static final ENV_DEFAULT = [
      endpointAPI: "http://localhost:8080",
      postsResource: "/posts",
      token: "lorem"
  ]

  private HttpResponse httpResponse;

  BlockingHttpClient blockingHttpClient = Stub(BlockingHttpClient.class)

  @Inject
  @Client("/")
  HttpClient client = Stub(HttpClient.class)  {
    toBlocking() >> blockingHttpClient
  }

  @Subject
  PostsRepository postsRepository = new APIJsonRepository(client,
      ENV_DEFAULT.endpointAPI,
      ENV_DEFAULT.postsResource,
      ENV_DEFAULT.token)

  static final responseMock =[{
                                1
                                1
                                "title"
                                "body"
                              }]


  def "should return information posts"() {
    given: " a response in call a API Json"

    blockingHttpClient.retrieve(_ as HttpRequest, List<PostsQuery>.class) >> responseMock

    when: "a get information about posts"
    def result = postsRepository.getPosts()

    then: "posts information is resolved"
    result == responseMock
  }

  def "should fail information posts when fail status code"() {
    given: "a fail response in call a API Json"
    HttpStatus httpStatus = httpStatusData
    httpResponse = new SimpleHttpResponse();
    httpResponse.body(responseMock);
    httpResponse.status(httpStatus)
    blockingHttpClient.retrieve(_ as HttpRequest, List<PostsQuery>.class) >> { throw new HttpClientResponseException(httpStatus.getReason(),httpResponse) }

    when: "a get all posts is requested"
    def result = postsRepository.getPosts()

    then: "information posts is resolved"
    result.size() == 0

    where: "The httpStatus is incorrect"
    httpStatusData << [HttpStatus.BAD_REQUEST,HttpStatus.CONNECTION_TIMED_OUT]
  }

}
