package com.clean.architecture.contexts.posts.application.find

import com.clean.architecture.contexts.posts.domain.PostsRepository
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification
import spock.lang.Subject

@MicronautTest(environments = ["test"])
class PostsFinderSpec extends Specification {

    static final POSTS_QUERY =[{
                                    1
                                    1
                                    "title"
                                    "body"
                                }]

    PostsRepository postsRepository = Mock()

    @Subject
    PostsFinder postsFinder = new PostsFinder(postsRepository)

    def "should find posts successfully"() {
        given: "exist more that one post"
        postsRepository.getPosts() >> POSTS_QUERY

        when: "requested all posts"
        def result = postsFinder.run()

        then: "post list should be returned"
        result.size()>0
    }
}
