package com.clean.architecture

import io.micronaut.context.ApplicationContext
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

@MicronautTest
class AppSpec extends Specification {

    @Shared
    @AutoCleanup
    EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer)

    @Inject
    EmbeddedServer server //refers to the server that was started up for this test suite

    @Inject
    ApplicationContext context

    def 'test it works'() {
        expect:
        embeddedServer.running
    }
    def 'test it works application'() {
        expect:
        Application.main(new String[] {})
    }

    def 'Should the Application have a hash code more that zero'() {
        def application = new Application()
        expect:
        application.hashCode() > 0
    }

}
