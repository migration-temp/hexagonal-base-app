package com.clean.architecture.api;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller
final class HealthController {
    @Get("/health")
    public String  health() {
        return "Healthy OK!";
    }
}
