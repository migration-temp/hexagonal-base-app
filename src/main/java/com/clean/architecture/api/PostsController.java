package com.clean.architecture.api;

import com.clean.architecture.contexts.posts.application.find.PostsFinder;
import com.clean.architecture.contexts.posts.domain.PostsQuery;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Controller
public final class PostsController{
    private static final Logger LOG = LoggerFactory.getLogger(PostsController.class);


    @Inject
    PostsFinder postsFinder;

    public PostsController(PostsFinder postsFinder) {
        this.postsFinder = postsFinder;
    }

    @Get("posts")
    public List<PostsQuery> find() {
        LOG.info("START FIND POSTS");
        return postsFinder.run();
    }
}
