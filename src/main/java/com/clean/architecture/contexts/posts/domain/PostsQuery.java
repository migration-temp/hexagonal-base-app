package com.clean.architecture.contexts.posts.domain;


import io.micronaut.core.annotation.Introspected;

@Introspected
public class PostsQuery {
    private Integer id;
    private Integer userId;
    private String title;
    private String body;
}
