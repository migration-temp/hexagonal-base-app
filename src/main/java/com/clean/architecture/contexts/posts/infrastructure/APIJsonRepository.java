package com.clean.architecture.contexts.posts.infrastructure;

import com.clean.architecture.contexts.posts.domain.PostsQuery;
import com.clean.architecture.contexts.posts.domain.PostsRepository;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.uri.UriBuilder;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static io.micronaut.http.HttpHeaders.CONTENT_TYPE;
import static io.micronaut.http.MediaType.APPLICATION_JSON;

@Singleton
public class APIJsonRepository implements PostsRepository {

    private static final Logger LOG = LoggerFactory.getLogger(APIJsonRepository.class + " Infrastructure Layer");

    @Inject
    HttpClient client;

    private String endpointAPI;
    private String postsResource;
    private String token;

    APIJsonRepository(HttpClient client,
                      @Value("${API.JSON_PLACE.url}")String endpointAPI,
                      @Value("${API.JSON_PLACE.posts.path}")String postsResource,
                      @Value("${API.JSON_PLACE.token}")String token) {
        this.client = client;
        this.endpointAPI = endpointAPI;
        this.postsResource = postsResource;
        this.token = token;
    }

    @Override
    public List<PostsQuery> getPosts() {
        List<PostsQuery> limitsQuery =  new ArrayList<>();
        try {
            LOG.info("GET POSTS  FROM API ({})->({}). NEW CALL ",endpointAPI,postsResource);
            String url = UriBuilder.of(endpointAPI.concat(postsResource)).toString();
            HttpRequest<?> req = HttpRequest.GET(url)
                    .header(CONTENT_TYPE, APPLICATION_JSON);

            limitsQuery = client.toBlocking().retrieve(req,List.class);
            LOG.info("END GET POSTS ({})->({}). SUCCESSFUL({}) ",
                    endpointAPI,
                    postsResource,
                    HttpStatus.OK.getCode());

        }catch(HttpClientResponseException e){
            LOG.debug("TO A STATUS({}) WITH DETAILS AS ({})",e.getStatus().getCode(), e.getMessage());
            LOG.error("END GET POSTS ({})->({}). FAILED({})",
                    endpointAPI,
                    postsResource,
                    e.getStatus().getCode());
        }
        return limitsQuery;
    }
}
