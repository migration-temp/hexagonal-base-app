package com.clean.architecture.contexts.posts.domain;

import java.util.List;

public interface PostsRepository {
    /**
     * This function to get all posts
     * @return Post List
     */
    List<PostsQuery> getPosts();
}
