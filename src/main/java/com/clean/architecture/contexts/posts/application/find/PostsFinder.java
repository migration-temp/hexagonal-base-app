package com.clean.architecture.contexts.posts.application.find;

import com.clean.architecture.contexts.posts.domain.PostsQuery;
import com.clean.architecture.contexts.posts.domain.PostsRepository;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Singleton
public class PostsFinder {
  private static final Logger LOG = LoggerFactory.getLogger(PostsFinder.class + " Application Layer");

  @Inject
  private final PostsRepository postsRepository;

  public PostsFinder(PostsRepository postsRepository) {
    this.postsRepository = postsRepository;
  }

  public List<PostsQuery> run() {
    LOG.info("USE CASE GET ALL POSTS");
    //TODO: Value Objects- Business rules applied intro domain layers and build Event Domain
    List<PostsQuery> posts = postsRepository.getPosts();
    //TODO: publisher Event Domain (👍/👎). Applying some EDA
    return posts;
  }
}
